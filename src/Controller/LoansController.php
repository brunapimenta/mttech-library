<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Exception\BadRequestException;
use Cake\View\Exception\MissingTemplateException;

class LoansController extends AppController
{
    public function index()
    {
        $this->set('title', 'Empréstimos');

        $loans = $this->Loans->find()
            ->enableHydration(false)
            ->contain(['Client', 'Book'])
            ->order(['Book.title']);

        $this->set('loans', $loans);
    }

    public function add()
    {
        $this->set('title', 'Cadastrar Empréstimo');

        $objLoan = $this->Loans->newEntity();

        $this->loadModel('Clients');
        $clients = $this->Clients->find('list')->order(['name']);
        $this->set('clients', $clients);

        $this->loadModel('Books');
        $books = $this->Books->find('list')->where(['quantity_available >' => 0])->order(['title']);
        $this->set('books', $books);

        if ($this->request->is(['patch', 'post', 'put'])) {
            try {
                $data = $this->request->getData();
                $data['id_user'] = $this->Auth->user('id_user');

                $objLoan = $this->Loans->patchEntity($objLoan, $data);

                if ($objLoan->getErrors()) {
                    throw new BadRequestException('Ocorreu um erro ao salvar os dados. Por favor, tente novamente.');
                }

                if (!$this->Loans->save($objLoan)) {
                    throw new BadRequestException('Ocorreu um erro ao salvar os dados. Por favor, tente novamente.');
                }

                $this->loadModel('Books');
                $objBook = $this->Books->get($objLoan->id_book);
                $objBook->quantity_available = (int) $objBook->quantity_available - 1;
                $this->Books->save($objBook);

                $this->Flash->success('Empréstimo cadastrado com sucesso.');
                return $this->redirect(['controller' => 'Loans', 'action' => 'index']);
            } catch (BadRequestException $e) {
                $this->Flash->error($e->getMessage());
            }
        }

        $this->set('loan', $objLoan);
    }

    public function edit($id)
    {
        if ($id === null) {
            throw new NotFoundException('Página não encontrada!');
        }

        $this->set('title', 'Editar Empréstimo');

        $objLoan = $this->Loans->get($id, ['contain' => ['Client', 'Book']]);

        if ($objLoan === false) {
            throw new NotFoundException('Página não encontrada!');
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            try {
                $objLoan = $this->Loans->patchEntity($objLoan, $this->request->getData());

                if ($objLoan->getErrors()) {
                    throw new BadRequestException('Ocorreu um erro ao salvar os dados. Por favor, tente novamente.');
                }

                if (!$this->Loans->save($objLoan)) {
                    throw new BadRequestException('Ocorreu um erro ao salvar os dados. Por favor, tente novamente.');
                }

                $msg = 'Empréstimo alterado com sucesso.';

                $this->loadModel('Books');
                $objBook = $this->Books->get($objLoan->id_book);
                if ($objBook->end !== null) {
                    $objBook->quantity_available = (int) $objBook->quantity_available + 1;
                    $msg = 'Empréstimo encerrado com sucesso.';
                }
                $this->Books->save($objBook);

                $this->Flash->success($msg);
            } catch (BadRequestException $e) {
                $this->Flash->error($e->getMessage());
            }
        }

        $this->set('loan', $objLoan);
    }
}
