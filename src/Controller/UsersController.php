<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['login', 'logout']);
    }

    public function login()
    {
        $this->viewBuilder()->layout('login');

        if ($this->Auth->user()) {
            return $this->redirect('/');
        }

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect('/');
            }

            $this->Flash->error('O e-mail e a senha não conferem. Por favor, tente novamente.');
        }

        $this->set('title', 'Entrar');
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }
}
