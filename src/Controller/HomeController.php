<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;

class HomeController extends AppController
{
    public function index()
    {
        $this->set('title', 'Início');
        $this->set('breadcrumbs', ['Início']);

        // $this->set('user', $this->Auth->user());
    }
}
