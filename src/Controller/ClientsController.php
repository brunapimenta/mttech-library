<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Exception\BadRequestException;
use Cake\View\Exception\MissingTemplateException;

class ClientsController extends AppController
{
    public function index()
    {
        $this->set('title', 'Clientes');

        $clients = $this->Clients->find()->order(['Clients.name']);

        $this->set('clients', $clients);
    }

    public function add()
    {
        $this->set('title', 'Novo Cliente');

        $objClient = $this->Clients->newEntity();

        if ($this->request->is(['patch', 'post', 'put'])) {
            try {
                $objClient = $this->Clients->patchEntity($objClient, $this->request->getData());

                if ($objClient->getErrors()) {
                    throw new BadRequestException('Ocorreu um erro ao salvar os dados. Por favor, tente novamente.');
                }

                if (!$this->Clients->save($objClient)) {
                    throw new BadRequestException('Ocorreu um erro ao salvar os dados. Por favor, tente novamente.');
                }

                $this->Flash->success('Cliente cadastrado com sucesso.');
                return $this->redirect(['controller' => 'Clients', 'action' => 'index']);
            } catch (BadRequestException $e) {
                $this->Flash->error($e->getMessage());
            }
        }

        $this->set('client', $objClient);
    }

    public function edit($id)
    {
        if ($id === null) {
            throw new NotFoundException('Página não encontrada!');
        }

        $this->set('title', 'Editar Cliente');

        $objClient = $this->Clients->get($id);

        if ($objClient === false) {
            throw new NotFoundException('Página não encontrada!');
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            try {
                $objClient = $this->Clients->patchEntity($objClient, $this->request->getData());

                if ($objClient->getErrors()) {
                    throw new BadRequestException('Ocorreu um erro ao salvar os dados. Por favor, tente novamente.');
                }

                if (!$this->Clients->save($objClient)) {
                    throw new BadRequestException('Ocorreu um erro ao salvar os dados. Por favor, tente novamente.');
                }

                $this->Flash->success('Cliente alterado com sucesso.');
            } catch (BadRequestException $e) {
                $this->Flash->error($e->getMessage());
            }
        }

        $this->set('client', $objClient);
    }

    public function delete($id)
    {
        $this->request->allowMethod(['post', 'delete']);
        try {
            if ($id === null) {
                throw new BadRequestException('Ocorreu um erro ao remover os dados. Por favor, tente novamente.');
            }

            $objClient = $this->Clients->get($id);

            if ($objClient === false) {
                throw new NotFoundException('Página não encontrada!');
            }

            if (!$this->Clients->delete($objClient)) {
                throw new BadRequestException('Ocorreu um erro ao remover os dados. Por favor, tente novamente.');
            }

            $this->Flash->success('Cliente removido com sucesso.');
        } catch (BadRequestException $e) {
            $this->Flash->error($e->getMessage());
        }

        return $this->redirect(['controller' => 'Clients', 'action' => 'index']);
    }
}
