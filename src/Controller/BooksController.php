<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Exception\BadRequestException;
use Cake\View\Exception\MissingTemplateException;

class BooksController extends AppController
{
    public function index()
    {
        $this->set('title', 'Livros');

        $books = $this->Books->find()->order(['Books.title']);

        $this->set('books', $books);
    }

    public function add()
    {
        $this->set('title', 'Novo Livro');

        $objBook = $this->Books->newEntity();

        if ($this->request->is(['patch', 'post', 'put'])) {
            try {
                $objBook = $this->Books->patchEntity($objBook, $this->request->getData());

                if ($objBook->getErrors()) {
                    throw new BadRequestException('Ocorreu um erro ao salvar os dados. Por favor, tente novamente.');
                }

                if (!$this->Books->save($objBook)) {
                    throw new BadRequestException('Ocorreu um erro ao salvar os dados. Por favor, tente novamente.');
                }

                $this->Flash->success('Livro cadastrado com sucesso.');
                return $this->redirect(['controller' => 'Books', 'action' => 'index']);
            } catch (BadRequestException $e) {
                $this->Flash->error($e->getMessage());
            }
        }

        $this->set('book', $objBook);
    }

    public function edit($id)
    {
        if ($id === null) {
            throw new NotFoundException('Página não encontrada!');
        }

        $this->set('title', 'Editar Livro');

        $objBook = $this->Books->get($id);

        if ($objBook === false) {
            throw new NotFoundException('Página não encontrada!');
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            try {
                $objBook = $this->Books->patchEntity($objBook, $this->request->getData());

                if ($objBook->getErrors()) {
                    throw new BadRequestException('Ocorreu um erro ao salvar os dados. Por favor, tente novamente.');
                }

                if (!$this->Books->save($objBook)) {
                    throw new BadRequestException('Ocorreu um erro ao salvar os dados. Por favor, tente novamente.');
                }

                $this->Flash->success('Livro alterado com sucesso.');
            } catch (BadRequestException $e) {
                $this->Flash->error($e->getMessage());
            }
        }

        $this->set('book', $objBook);
    }

    public function delete($id)
    {
        $this->request->allowMethod(['post', 'delete']);
        try {
            if ($id === null) {
                throw new BadRequestException('Ocorreu um erro ao remover os dados. Por favor, tente novamente.');
            }

            $objBook = $this->Books->get($id);

            if ($objBook === false) {
                throw new NotFoundException('Página não encontrada!');
            }

            if (!$this->Books->delete($objBook)) {
                throw new BadRequestException('Ocorreu um erro ao remover os dados. Por favor, tente novamente.');
            }

            $this->Flash->success('Livro removido com sucesso.');
        } catch (BadRequestException $e) {
            $this->Flash->error($e->getMessage());
        }

        return $this->redirect(['controller' => 'Books', 'action' => 'index']);
    }
}
