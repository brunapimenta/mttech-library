<?php
namespace App\View\Helper;
use Cake\View\Helper;
use Cake\View\View;

class FormHelper extends Helper\FormHelper
{
    private $templates = [
        'dateWidget' => '<span class="form-inline">{{year}}{{month}}{{day}}{{hour}}{{minute}}{{second}}{{meridian}}</span>',
        'error' => '<div class="text-danger">{{content}}</div>',
        'inputContainer' => '<div class="form-group {{type}}{{required}}">{{content}}</div>',
        'inputContainerError' => '<div class="form-group {{type}}{{required}} has-error">{{content}}{{error}}</div>',
    ];

    private $templates_horizontal = [
        'label' => '<label class="control-label col-md-2"{{attrs}}>{{text}}</label>',
        'formGroup' => '{{label}}<div class=" col-md-10">{{input}}{{error}}{{help}}</div>',
        'checkboxFormGroup' => '<div class="checkbox">{{label}}</div>{{error}}{{help}}',
        'submitContainer' => '<div class="col-md-10 col-md-offset-2">{{content}}</div>',
        'inputContainer' => '<div class="form-group {{type}}{{required}}">{{content}}</div>',
        'inputContainerError' => '<div class="form-group {{type}}{{required}} has-error">{{content}}</div>',
    ];

    public function __construct(View $View, array $config = [])
    {
        $this->_defaultConfig['templates'] = array_merge($this->_defaultConfig['templates'], $this->templates);
        parent::__construct($View, $config);
    }
}
