<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Editar Cliente</h1>
</div>

<section class="content">
    <?= $this->Form->create($client) ?>
        <p>Os campos marcados com <span class="required">*</span> são de preenchimento obrigatório.</p>
        <?= $this->Form->control('name', ['label' => 'Nome <span class="required">*</span>', 'class' => 'form-control', 'required' => true, 'maxlength' => 255, 'escape' => false]) ?>

        <hr>

        <?= $this->Html->link('Voltar', ['controller' => 'Clients', 'action' => 'index'], ['class' => 'btn btn-outline-secondary', 'escape' => false]); ?>
        <button type="submit" class="btn btn-primary float-right">Salvar</button>
    <?= $this->Form->end() ?>
</section>
