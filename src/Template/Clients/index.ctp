<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">
        Clientes
        <?= $this->Html->link('<i data-feather="plus"></i> Novo Cliente', ['controller' => 'Clients', 'action' => 'add'], ['class' => 'btn btn-outline-primary btn-sm', 'escape' => false]) ?>
    </h1>
</div>

<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div class="table-responsive">
                <table class="dataTables table table-striped table-bordered table-sm">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nome</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($clients as $client) {
                                echo '<tr>';
                                    echo '<td>' . $client['id_client'] . '</td>';
                                    echo '<td>';
                                        echo $this->Html->link($client['name'], ['controller' => 'Clients', 'action' => 'edit', $client['id_client']]);
                                    echo '</td>';
                                echo '</tr>';
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
