<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert alert-danger" role="alert" style="margin-top: 15px;margin-bottom: 10px;">
    <h4 class="alert-heading"><i data-feather="alert-triangle"></i> Ops! Ocorreu um erro!</h4>
    <p style="font-size: 16px;"><?= $message ?></p>
</div>
