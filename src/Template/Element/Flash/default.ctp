<?php
$class = 'message';
if (!empty($params['class'])) {
    $class .= ' ' . $params['class'];
}
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert alert-info" role="alert" style="margin-top: 15px;margin-bottom: 15px;">
    <h4 class="alert-heading"><i data-feather="alert-circle"></i> Atenção!</h4>
    <p style="font-size: 16px;"><?= $message ?></p>
</div>
