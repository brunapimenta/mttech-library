<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert alert-success" role="alert" style="margin-top: 15px;margin-bottom: 10px;">
    <h4 class="alert-heading"><i data-feather="check"></i> Feito!</h4>
    <p style="font-size: 16px;"><?= $message ?></p>
</div>
