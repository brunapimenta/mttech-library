<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Novo Livro</h1>
</div>

<section class="content">
    <?= $this->Form->create($book) ?>
        <p>Os campos marcados com <span class="required">*</span> são de preenchimento obrigatório.</p>
        <?= $this->Form->control('title', ['label' => 'Título <span class="required">*</span>', 'class' => 'form-control', 'required' => true, 'maxlength' => 255, 'escape' => false]) ?>
        <div class="row">
            <div class="col-sm-6">
                <?= $this->Form->control('author', ['label' => 'Autor(a) <span class="required">*</span>', 'class' => 'form-control', 'required' => true, 'maxlength' => 255, 'escape' => false]) ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->control('quantity_available', ['type' => 'number', 'label' => 'Quantidade Disponível <span class="required">*</span>', 'class' => 'form-control', 'required' => true, 'minlength' => 0, 'escape' => false]) ?>
            </div>
        </div>

        <hr>

        <?= $this->Html->link('Voltar', ['controller' => 'Books', 'action' => 'index'], ['class' => 'btn btn-outline-secondary', 'escape' => false]); ?>
        <button type="submit" class="btn btn-primary float-right">Salvar</button>
    <?= $this->Form->end() ?>
</section>
