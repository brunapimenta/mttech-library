<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">
        Livros
        <?= $this->Html->link('<i data-feather="plus"></i> Novo Livro', ['controller' => 'Books', 'action' => 'add'], ['class' => 'btn btn-outline-primary btn-sm', 'escape' => false]) ?>
    </h1>
</div>

<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div class="table-responsive">
                <table class="dataTables table table-striped table-bordered table-sm">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Título</th>
                            <th>Autor(a)</th>
                            <th>Quantidade Disponível</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($books as $book) {
                                echo '<tr>';
                                    echo '<td>' . $book['id_book'] . '</td>';
                                    echo '<td>';
                                        echo $this->Html->link($book['title'], ['controller' => 'Books', 'action' => 'edit', $book['id_book']]);
                                    echo '</td>';
                                    echo '<td>' . $book['author'] . '</td>';
                                    echo '<td>' . $book['quantity_available'] . '</td>';
                                echo '</tr>';
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
