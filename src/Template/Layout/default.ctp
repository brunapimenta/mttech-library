<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'MTLibrary';
$this->assign('title', $title);
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?> |
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('bootstrap.min.css') ?>
    <?= $this->Html->css('select2.min.css') ?>
    <?= $this->Html->css('select2.bootstrap4.min.css') ?>
    <?= $this->Html->css('template.css') ?>
    <?= $this->Html->css('dataTables.bootstrap4.min.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="/">
            <?= $cakeDescription ?>
        </a>
        <!-- <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search"> -->
        <ul class="navbar-nav px-3">
            <li class="nav-item text-nowrap">
                <?= $this->Html->link('Sair', ['controller' => 'Users', 'action' => 'logout'], ['class' => 'nav-link']) ?>
            </li>
        </ul>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                <div class="sidebar-sticky">
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <?= $this->Html->link('<i data-feather="home"></i> Início', '/', ['class' => 'nav-link', 'escape' => false]) ?>
                        </li>
                        <li class="nav-item">
                            <?= $this->Html->link('<i data-feather="book"></i> Livros', ['controller' => 'Books', 'action' => 'index'], ['class' => 'nav-link', 'escape' => false]) ?>
                        </li>
                        <li class="nav-item">
                            <?= $this->Html->link('<i data-feather="users"></i> Clientes', ['controller' => 'Clients', 'action' => 'index'], ['class' => 'nav-link', 'escape' => false]) ?>
                        </li>
                        <li class="nav-item">
                            <?= $this->Html->link('<i data-feather="git-branch"></i> Empréstimos', ['controller' => 'Loans', 'action' => 'index'], ['class' => 'nav-link', 'escape' => false]) ?>
                        </li>
                    </ul>
                </div>
            </nav>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                <?= $this->Flash->render() ?>
                <?= $this->fetch('content') ?>
            </main>
        </div>
    </div>

    <?= $this->Html->script('jquery-2.2.4.min.js') ?>
    <?= $this->Html->script('bootstrap.min.js') ?>
    <?= $this->Html->script('feather.min.js') ?>
    <?= $this->Html->script('select2.full.min.js') ?>
    <?= $this->Html->script('select2.pt-BR.js') ?>
    <?= $this->Html->script('jquery.dataTables.min.js') ?>
    <?= $this->Html->script('dataTables.bootstrap4.min.js') ?>
    <?= $this->Html->script('inputmask.min.js') ?>
    <?= $this->Html->script('inputmask.date.extensions.min.js') ?>
    <?= $this->Html->script('inputmask.extensions.min.js') ?>
    <?= $this->Html->script('jquery.inputmask.min.js') ?>

    <script>
        feather.replace();

        window.dataTableOptions = {
            responsive: true,
            order: [],
            oLanguage: {
                sProcessing: 'Processando...',
                sLengthMenu: 'Mostrar _MENU_ registros',
                sZeroRecords: 'Não foram encontrados resultados',
                sInfo: 'Mostrando de _START_ até _END_ de _TOTAL_ registros',
                sInfoEmpty: 'Mostrando de 0 até 0 de 0 registros',
                sInfoFiltered: '(filtrado de _MAX_ registros no total)',
                sInfoPostFix: '',
                sSearch: 'Buscar:',
                sUrl: '',
                oPaginate: {
                    sFirst: 'Primeiro',
                    sPrevious: 'Anterior',
                    sNext: 'Seguinte',
                    sLast: 'Último'
                }
            },
            columnDefs: [{
                sortable: true,
                visible: true,
            }],
        };

        $(document).ready(function() {
            var a = $('a[href="<?php echo $this->request->getAttribute('webroot') . $this->request->getRequestTarget('url') ?>"]');
            request = '<?php echo $this->request->getRequestTarget('url') ?>';
            request = request.split('/');

            if (a.length === 0 && request.length > 1) {
                a = $('a[href="<?php echo $this->request->getAttribute('webroot') ?>' + request[1] + '"]');
            }

            a.addClass('active');

            $('button[type="submit"]').attr('data-loading-text', "Aguarde...");

            $('form').on('submit', function(e) {
                $('button[type="submit"]').button('loading');
            });

            if ($('.has-error').length > 0 && $('.has-error').find('input').length > 0) {
                $('.has-error').find('input').focus();
            }

            $('.select2').select2({theme: "bootstrap4", language: "pt-BR"});
            $('.modal').modal({backdrop: 'static', keyboard: false, show: false});
            $('.dataTables').DataTable(window.dataTableOptions);
            $('.mask-datetime').inputmask({alias: 'datetime'});
        });
    </script>
</body>
</html>
