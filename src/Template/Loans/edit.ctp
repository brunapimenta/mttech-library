<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Atualizar Empréstimo</h1>
</div>

<section class="content">
    <?= $this->Form->create($loan) ?>
        <p>Os campos marcados com <span class="required">*</span> são de preenchimento obrigatório.</p>

        <div class="form-group">
            <label for="staticClient">Cliente <span class="required">*</span></label>
            <input type="text" class="form-control-plaintext" id="staticClient" value="<?= $loan['client']['name'] ?>" readonly>
        </div>

        <div class="form-group">
            <label for="staticBook">Livro <span class="required">*</span></label>
            <input type="text" class="form-control-plaintext" id="staticBook" value="<?= $loan['book']['title'] ?>" readonly>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <?= $this->Form->control('start', ['type' => 'text', 'label' => 'Retirada em <span class="required">*</span>', 'class' => 'form-control mask-datetime', 'value' => $loan['start']->format('d/m/Y H:i'), 'required' => true, 'escape' => false]) ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->control('end', ['type' => 'text', 'label' => 'Devolução em', 'class' => 'form-control mask-datetime', 'value' => ($loan['end'] !== null ? $loan['end']->format('d/m/Y H:i') : ''), 'required' => false, 'escape' => false]) ?>
            </div>
        </div>

        <hr>

        <?= $this->Html->link('Voltar', ['controller' => 'Loans', 'action' => 'index'], ['class' => 'btn btn-outline-secondary', 'escape' => false]); ?>
        <button type="submit" class="btn btn-primary float-right">Salvar</button>
    <?= $this->Form->end() ?>
</section>
