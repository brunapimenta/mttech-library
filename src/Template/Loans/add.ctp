<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Novo Empréstimo</h1>
</div>

<section class="content">
    <?= $this->Form->create($loan) ?>
        <p>Os campos marcados com <span class="required">*</span> são de preenchimento obrigatório.</p>
        <?= $this->Form->control('id_client', ['type' => 'select', 'label' => 'Cliente <span class="required">*</span>', 'class' => 'form-control select2', 'options' => $clients, 'empty' => 'Selecione', 'required' => true, 'escape' => false]) ?>

        <?= $this->Form->control('id_book', ['type' => 'select', 'label' => 'Livro <span class="required">*</span>', 'class' => 'form-control select2', 'options' => $books, 'empty' => 'Selecione', 'required' => true, 'escape' => false]) ?>

        <div class="row">
            <div class="col-sm-6">
                <?= $this->Form->control('start', ['type' => 'text', 'label' => 'Retirada em <span class="required">*</span>', 'class' => 'form-control mask-datetime', 'required' => true, 'escape' => false]) ?>
            </div>
            <div class="col-sm-6">
                <?= $this->Form->control('end', ['type' => 'text', 'label' => 'Devolução em', 'class' => 'form-control mask-datetime', 'required' => false, 'escape' => false]) ?>
            </div>
        </div>

        <hr>

        <?= $this->Html->link('Voltar', ['controller' => 'Loans', 'action' => 'index'], ['class' => 'btn btn-outline-secondary', 'escape' => false]); ?>
        <button type="submit" class="btn btn-primary float-right">Salvar</button>
    <?= $this->Form->end() ?>
</section>
