<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">
        Empréstimos
        <?= $this->Html->link('<i data-feather="plus"></i> Novo Empréstimo', ['controller' => 'Loans', 'action' => 'add'], ['class' => 'btn btn-outline-primary btn-sm', 'escape' => false]) ?>
    </h1>
</div>

<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div class="table-responsive">
                <table class="dataTables table table-striped table-bordered table-sm">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Cliente</th>
                            <th>Livro</th>
                            <th>Retirada em</th>
                            <th>Devolução em</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($loans as $loan) {
                                echo '<tr>';
                                    echo '<td>';
                                        echo $this->Html->link($loan['id_client_book'], ['controller' => 'Loans', 'action' => 'edit', $loan['id_client_book']]);
                                    echo '</td>';
                                    echo '<td>' . $loan['client']['name'] . '</td>';
                                    echo '<td>' . $loan['book']['title'] . '</td>';
                                    echo '<td>' . $loan['start']->format('d/m/Y H:i') . '</td>';
                                    echo '<td>' . ($loan['end'] !== null ? $loan['end']->format('d/m/Y H:i') : '-') . '</td>';
                                echo '</tr>';
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
