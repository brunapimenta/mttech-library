<?php echo $this->Form->create('User', ['class' => 'form-signin']); ?>
    <?= $this->Html->image('logo_rdu.png', ['class' => 'img-fluid', 'style' => 'width:150px;height:150px;']); ?>
    <h1 class="h3 mb-3 font-weight-normal">Entrar</h1>

    <?= $this->Flash->render() ?>

    <label for="inputEmail" class="sr-only">E-mail</label>
    <?= $this->Form->control('email', ['type' => 'email', 'label' => false, 'id' => 'inputEmail', 'class' => 'form-control', 'placeholder' => 'E-mail', 'required' => true]) ?>

    <label for="inputPassword" class="sr-only">Senha</label>
    <?= $this->Form->control('password', ['type'=> 'password', 'label' => false, 'id' => 'inputPassword', 'class' => 'form-control', 'placeholder' => 'Senha', 'required' => true]) ?>

    <button class="btn btn-lg btn-primary btn-block" type="submit">Acessar</button>

    <p class="mt-5 mb-3 text-muted">&copy; <?= date('Y') ?></p>
<?= $this->Form->end() ?>
