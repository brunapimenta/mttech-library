<?php
namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use DateTime;

class DateBehavior extends Behavior
{
    public function convert($value, $from = 'd/m/Y', $to = 'Y-m-d')
    {
        if (trim($value) === '') {
            return null;
        }

        $date = DateTime::createFromFormat($from, $value);

        if ($date === false) {
            return $value;
        }

        return $date->format($to);
    }
}
