<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use ArrayObject;

class ClientsTable extends Table
{
    public function initialize(array $config)
    {
        $this->setPrimaryKey('id_client');
        $this->setDisplayField('name');

        $this->hasMany('Books', [
            'className' => 'ClientBooks',
            'foreignKey' => 'id_client',
            'targetForeignKey' => 'id_client',
            'bindingKey' => 'id_client'
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id_client')
            ->allowEmpty('id_client', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name', 'O cliente é obrigatório.');

        return $validator;
    }

    public function beforeMarshal(Event $event, ArrayObject $data)
    {
        $this->nullableFields($data);
    }

    private function nullableFields(ArrayObject $data)
    {
        foreach ($data as $index => &$dt) {
            if (is_array($dt)) {
                if (empty($dt)) {
                    $dt = null;
                }
            } else if (trim($dt) == '') {
                $dt = null;
            } else {
                $dt = trim($dt);
            }
        }
    }
}
