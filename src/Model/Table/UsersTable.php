<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setPrimaryKey('id_user');
        $this->setDisplayField('email');
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id_user')
            ->allowEmpty('id_user', 'create');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email', 'O e-mail é obrigatório.');

        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password', 'A senha é obrigatória.')
            ->add('password', [
                'minLength' => [
                    'rule' => ['minLength', 4],
                    'last' => true,
                    'message' => 'A senha deve ter entre 4 e 8 caracteres.'
                ]
            ]);

        return $validator;
    }
}
