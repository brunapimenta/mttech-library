<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\RulesChecker;
use Cake\Event\Event;
use ArrayObject;

class LoansTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('client_books');
        $this->setPrimaryKey('id_client_book');
        $this->setDisplayField('start');

        $this->addBehavior('Date');

        $this->hasOne('Client', [
            'className' => 'Clients',
            'foreignKey' => 'id_client',
            'targetForeignKey' => 'id_client',
            'bindingKey' => 'id_client',
            'joinType' => 'INNER'
        ]);

        $this->hasOne('Book', [
            'className' => 'Books',
            'foreignKey' => 'id_book',
            'targetForeignKey' => 'id_book',
            'bindingKey' => 'id_book',
            'joinType' => 'INNER'
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id_client_book')
            ->allowEmpty('id_client_book', 'create');

        $validator
            ->integer('id_client')
            ->requirePresence('id_client', 'create')
            ->notEmpty('id_client', 'O cliente é obrigatório.');

        $validator
            ->integer('id_book')
            ->requirePresence('id_book', 'create')
            ->notEmpty('id_book', 'O livro é obrigatório.');

        $validator
            ->datetime('start')
            ->requirePresence('start', 'create')
            ->notEmpty('start', 'A data de início do empréstimo é obrigatória.');

        $validator
            ->datetime('end')
            ->allowEmpty('end');

        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['id_client'], 'Client', 'O cliente informado não existe.'));
        $rules->add($rules->existsIn(['id_book'], 'Book', 'O livro informado não existe.'));

        return $rules;
    }

    public function beforeMarshal(Event $event, ArrayObject $data)
    {
        $this->nullableFields($data);

        if ($data['start'] != null && trim($data['start']) != '') {
            $data['start'] = $this->convert($data['start'], 'd/m/Y H:i', 'Y-m-d H:i');
        }

        if ($data['end'] != null && trim($data['end']) != '') {
            $data['end'] = $this->convert($data['end'], 'd/m/Y H:i', 'Y-m-d H:i');
        }
    }

    private function nullableFields(ArrayObject $data)
    {
        foreach ($data as $index => &$dt) {
            if (is_array($dt)) {
                if (empty($dt)) {
                    $dt = null;
                }
            } else if (trim($dt) == '') {
                $dt = null;
            } else {
                $dt = trim($dt);
            }
        }
    }
}
