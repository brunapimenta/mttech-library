<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\RulesChecker;
use Cake\Event\Event;
use ArrayObject;

class BooksTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setPrimaryKey('id_book');
        $this->setDisplayField('title');

        $this->hasMany('Loans', [
            'className' => 'Loans',
            'foreignKey' => 'id_book',
            'targetForeignKey' => 'id_book',
            'bindingKey' => 'id_book',
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id_book')
            ->allowEmpty('id_book', 'create');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title', 'O título é obrigatório.');

        $validator
            ->requirePresence('author', 'create')
            ->notEmpty('author', 'O autor é obrigatório.');

        $validator
            ->integer('quantity_available')
            ->requirePresence('quantity_available', 'create')
            ->notEmpty('quantity_available', 'A quantidade disponível é obrigatória.');

        return $validator;
    }

    public function beforeMarshal(Event $event, ArrayObject $data)
    {
        $this->nullableFields($data);
    }

    private function nullableFields(ArrayObject $data)
    {
        foreach ($data as $index => &$dt) {
            if (is_array($dt)) {
                if (empty($dt)) {
                    $dt = null;
                }
            } else if (trim($dt) == '') {
                $dt = null;
            } else {
                $dt = trim($dt);
            }
        }
    }
}
