# CakePHP Application Skeleton

A skeleton for creating applications with [CakePHP](https://cakephp.org) 3.6.

The framework source code can be found here: [cakephp/cakephp](https://github.com/cakephp/cakephp).

## Installation

1. Download [Composer](https://getcomposer.org/doc/00-intro.md) or update `composer self-update`.
2. Run `php composer.phar install`.

If Composer is installed globally, run

```bash
composer install
```

You can now either use your machine's webserver to view the default home page, or start
up the built-in webserver with:

```bash
bin/cake server -p 8765
```

Then visit `http://localhost:8765` to see the welcome page.

## Configuration

Read and edit `config/app.php` and setup the `'Datasources'` and any other
configuration relevant for your application. Remember to disabled the debug mode.

## Database

MySQL. The database's file dump is `DB - Dump20180418_1140.sql`, there we have the structure and some data inserts. Initially the name of database is `library`, but you can called it whatever you want, just keep the tables' name.

## Main Access

Nowadays the main access to the system is:
E-mail: `admin@admin.com`
Password: `admin`

## Layout

The app uses [Bootstrap](https://getbootstrap.com) 4.

## Created by

Bruna Pimenta.
